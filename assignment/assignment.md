Given a number of publications you can try to determine topics by clustering using a metric based on similarity of words occurring in titles. When you do this over several years you can get a feeling of how topics of interest shift over time.

Analyze for instance one of the following scientific fields (by extracting the titels of all relevant publications from the DBLP dataset):

    Data Mining: [KDD](http://dblp.uni-trier.de/db/conf/kdd/index.html), [PKDD](http://dblp.uni-trier.de/db/conf/pkdd/index.html), [ICDM](http://dblp.uni-trier.de/db/conf/icdm/index.html), [SDM](http://dblp.uni-trier.de/db/conf/sdm/sdm2012.html)

    Database Systems: [SIGMOD](http://dblp.uni-trier.de/db/conf/sigmod/index.html), [VLDB](http://dblp.uni-trier.de/db/conf/vldb/index.html), [EDBT](http://dblp.uni-trier.de/db/conf/edbt/), [ICDE](http://dblp.uni-trier.de/db/conf/icde/)

You should write a program for finding topics and use it to show how topics within the field or Data Mining and topics in the field of Database Systems shift over time. Use intervals of, say, 10 years but allow for some overlap. For every period show what the most relevant topics are. Visualize your results.

You should hand in your code together with a report containing the results of your analysis, an explanation of the followed approach and the difficulties you encountered.

Tip: make use of existing clustering algorithms in [SciPy](http://docs.scipy.org/doc/scipy/reference/cluster.html) or [Scikit-learn](http://scikit-learn.org/stable/modules/clustering.html).
