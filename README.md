# Requirements

- The dependencies in `requirements.txt`.
- Python 3 (tested using Python 3.8.0)
- Rust compiler and Cargo package manager

# Running the code

First, download the dblp dataset and paste it in this repository as dblp.xml.

```bash
pip install -r requirements.txt
./run.sh
```
