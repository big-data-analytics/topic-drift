# Convert source data into a CSV for better parsing in Python
if [ -f data_mining.csv ] && [ -f database_systems.csv ]; then
    echo "CSV files found. Not gonna build them again. Remove them to force rebuild."
else
    echo "Building CSV"
    if command -v cargo; then
        cd csv
        cargo run --release
        cd ..
    else
        echo "Please install a Rust compiler first. Instructions can be found on rust-lang.org"
        exit 1
    fi
fi

# Run the actual Python code
python3 src/main.py $1 3
