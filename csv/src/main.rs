mod db_reader;

use std::{
    fs::File,
    io::{prelude::*, BufWriter},
};

pub const DATA_MINING_CSV_PATH: &str = "../data_mining.csv";
pub const DB_SYS_CSV_PATH: &str = "../database_systems.csv";
pub const DATA_MINING_TOPICS: [&str; 4] = ["KDD", "PKDD", "ICDM", "SDM"];
pub const DB_SYS_TOPICS: [&str; 4] = ["SIGMOD", "VLDB", "EDBT", "ICDE"];

fn write_book(
    book: &db_reader::Book,
    mut bufr: BufWriter<File>,
) -> Result<BufWriter<File>, Box<dyn std::error::Error>> {
    bufr.write(&book.year.unwrap().to_string().into_bytes())?;
    bufr.write(b";")?;
    bufr.write(&book.title.as_ref().unwrap())?;
    bufr.write(b"\n")?;

    Ok(bufr)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file = db_reader::BooksReader::from_path(db_reader::DEFAULT_DB_PATH).unwrap();

    let data_mining_output = std::fs::File::create(DATA_MINING_CSV_PATH).unwrap();
    let mut data_mining_bufr = BufWriter::new(data_mining_output);

    let db_sys_output = std::fs::File::create(DB_SYS_CSV_PATH).unwrap();
    let mut db_sys_bufr = BufWriter::new(db_sys_output);

    for book in input_file {
        match book {
            Ok(book) => {
                if book.title.is_some() && book.year.is_some() && book.booktitle.is_some() {
                    let booktitle: String =
                        String::from_utf8_lossy(&book.booktitle.as_ref().unwrap()).into();
                    let mut written = false;

                    for data_mining_topic in DATA_MINING_TOPICS.iter() {
                        if !written && booktitle.contains(data_mining_topic) {
                            data_mining_bufr = write_book(&book, data_mining_bufr)?;
                            written = true;
                        }
                    }
                    for db_sys_topic in DB_SYS_TOPICS.iter() {
                        if !written && booktitle.contains(db_sys_topic) {
                            db_sys_bufr = write_book(&book, db_sys_bufr)?;
                            written = true;
                        }
                    }
                } else {
                    // Terminal output was slowing us down
                    // if let Some(strg) = book.title {
                    //     if strg != b"Home Page" {
                    //         println!(
                    //             "Warning: ignoring {} because it has no year or booktitle (topic) \
                    //              specified.",
                    //             String::from_utf8(strg).unwrap()
                    //         );
                    //     }
                    // }
                }
            }
            Err(err) => eprintln!("{}", err),
        }
        // TODO: this
    }

    Ok(())
}
