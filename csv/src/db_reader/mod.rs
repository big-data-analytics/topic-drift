use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use quick_xml::Reader;

pub const DEFAULT_DB_PATH: &str = "../dblp.xml";

pub struct Book {
    pub title: Option<Vec<u8>>,
    pub booktitle: Option<Vec<u8>>,
    pub year: Option<u64>,
}

impl Book {
    fn empty() -> Self {
        Book {
            title: None,
            booktitle: None,
            year: None,
        }
    }
}

pub struct BooksReader<R: BufRead> {
    xml_file_reader: Reader<R>,
    bufr: Vec<u8>,
}

impl<R: BufRead> Iterator for BooksReader<R> {
    /// Type used to read towards
    type Item = Result<Book, String>;

    /// Returns the next book, or "basket" in freq set terms.
    fn next(&mut self) -> Option<Self::Item> {
        use quick_xml::events::Event::*;
        loop {
            match self.xml_file_reader.read_event(&mut self.bufr) {
                Ok(Start(ref tag)) => match tag.name() {
                    b"article" | b"inproceedings" | b"proceedings" | b"book" | b"incollection"
                    | b"phdthesis" | b"mastersthesis" | b"www" => {
                        break Some(parse_book(&mut self.xml_file_reader, &mut self.bufr))
                    }
                    err_tag => {
                        break Some(Err(format!(
                            "Unexpected opening tag: {}",
                            String::from_utf8_lossy(err_tag)
                        )))
                    }
                },
                Ok(Eof) | Ok(End(_)) => break None,
                // These should not be called
                Ok(DocType(ref tag)) | Ok(Text(ref tag)) | Ok(PI(ref tag)) => {
                    break Some(Err(format!(
                        "Unexpected event: {}",
                        String::from_utf8_lossy(tag)
                    )))
                }
                Ok(Empty(ref tag)) => {
                    break Some(Err(format!(
                        "Unexpected event: {}",
                        String::from_utf8_lossy(tag)
                    )))
                }
                Ok(Decl(_)) | Ok(CData(_)) | Ok(Comment(_)) => continue,
                Err(err) => break Some(Err(format!("Error when reading XML: {:?}", err))),
            }
        }
    }
}

impl<R: BufRead> BooksReader<R> {
    pub fn from_read(read: R) -> Result<Self, String> {
        use quick_xml::events::Event::*;
        let mut reader = Reader::from_reader(read);
        reader.trim_text(true);
        reader.check_comments(false);
        reader.trim_markup_names_in_closing_tags(true);

        // Open the document
        let mut buf = Vec::new();

        // Open the root element
        loop {
            if let Ok(Start(ref tag)) = reader.read_event(&mut buf) {
                if tag.name() == b"dblp" {
                    break;
                }
            }
        }

        Ok(BooksReader {
            xml_file_reader: reader,
            bufr: buf,
        })
    }
}

impl BooksReader<BufReader<std::fs::File>> {
    /// Opens database from the given path
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self, String> {
        let file = File::open(&path).map_err(|e| {
            format!(
                "Failed to open file from path {}: {}",
                path.as_ref().display(),
                e
            )
        })?;
        let bufread = BufReader::new(file);

        Self::from_read(bufread)
    }
}

fn parse_book<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<Book, String> {
    use quick_xml::events::Event::*;

    let mut book_c = Book::empty();

    loop {
        match parser.read_event(buf) {
            Ok(Start(ref tag)) => {
                match tag.name() {
                    b"title" => {
                        let title = parse_title(parser, buf)?;
                        book_c.title = Some(title);
                    }
                    b"year" => {
                        let year = parse_year(parser, buf)?;
                        book_c.year = Some(year);
                    }
                    b"booktitle" => {
                        let booktitle: Vec<u8> = parse_booktitle(parser, buf)?;
                        book_c.booktitle = Some(booktitle);
                    }
                    b"author" | b"pages" | b"month" | b"journal" | b"volume" | b"number"
                    | b"url" | b"ee" | b"cite" | b"school" | b"publisher" | b"note" | b"cdrom"
                    | b"crossref" | b"isbn" | b"chapter" | b"series" | b"publnr" | b"editor"
                    | b"address" => {
                        // As these are not required for any assignment so far, these values are not
                        // kept.
                        if let Err(err) = parse_unimportant(parser, buf) {
                            return Err(err);
                        }
                    }
                    tagname => {
                        return Err(format!(
                            "Unexpected opening tag: {}",
                            String::from_utf8_lossy(tagname)
                        ))
                    }
                }
            }
            Ok(End(_)) => {
                buf.clear();
                return Ok(book_c);
            }
            Err(err) => return Err(format!("Error reading book: {}", err)),
            Ok(event) => return Err(format!("Unexpected while reading book: {:?}", event)),
        }
    }
}

fn parse_title<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<Vec<u8>, String> {
    use quick_xml::events::Event::*;

    let mut title: Vec<u8> = Vec::new();
    loop {
        match parser.read_event(buf) {
            Ok(Text(name)) => title.append(&mut name.into_owned().to_vec()),
            Ok(End(_)) => break,
            Ok(Start(_)) => {
                parse_xml_hell(parser, buf, &mut title)?;
            }
            Ok(event) => {
                return Err(format!(
                    "Expected end of publication, got {}",
                    String::from_utf8(event.into_owned().to_vec()).unwrap()
                ))
            }
            Err(err) => {
                eprintln!("");
                return Err(format!("Expected end of publication, got error {}", err));
            }
        }
    }

    Ok(title.into_iter().filter(|byte| *byte != b';').collect())
}

fn parse_year<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<u64, String> {
    use quick_xml::events::Event::*;

    // Read the name
    let year = match parser.read_event(buf) {
        Ok(Text(name)) => name.into_owned().to_vec(),
        Ok(event) => return Err(format!("Expected year of publication, got {:?}", event)),
        Err(err) => return Err(format!("Expected year of publication, got error {}", err)),
    };

    // Read the closing tag
    match parser.read_event(buf) {
        Ok(End(_)) => {}
        Ok(event) => return Err(format!("Expected end of publication date, got {:?}", event)),
        Err(err) => {
            return Err(format!(
                "Expected end of publication date, got error {}",
                err
            ))
        }
    }

    Ok(String::from_utf8(year).unwrap().parse().unwrap())
}

fn parse_booktitle<R: BufRead>(
    parser: &mut Reader<R>,
    buf: &mut Vec<u8>,
) -> Result<Vec<u8>, String> {
    use quick_xml::events::Event::*;

    let mut booktitle: Vec<u8> = Vec::new();
    loop {
        match parser.read_event(buf) {
            Ok(Text(name)) => booktitle.append(&mut name.into_owned().to_vec()),
            Ok(End(_)) => break,
            Ok(Start(_)) => {
                parse_xml_hell(parser, buf, &mut booktitle)?;
            }
            Ok(event) => {
                return Err(format!(
                    "Expected end of booktitle, got {}",
                    String::from_utf8(event.into_owned().to_vec()).unwrap()
                ))
            }
            Err(err) => {
                eprintln!("");
                return Err(format!("Expected end of booktitle, got error {}", err));
            }
        }
    }

    Ok(booktitle)
}

/// Parses nested XML tags that do not require nesting.
///
/// # Example
///
/// ```
/// <title><sub>a</sub></title> -> <title>a</title>
/// ```
fn parse_xml_hell<R: BufRead>(
    parser: &mut Reader<R>,
    buf: &mut Vec<u8>,
    text: &mut Vec<u8>,
) -> Result<(), String> {
    use quick_xml::events::Event::*;
    let mut depth = 1;
    while depth > 0 {
        let parse = parser.read_event(buf);
        match parse {
            Ok(Text(new_txt)) => {
                text.append(&mut new_txt.into_owned().to_vec());
            }
            Ok(Start(_)) => depth += 1,
            Ok(End(_)) => depth -= 1,
            Ok(Eof) => {
                return Err("Unexpected end of file in parsing unimportant things".to_string())
            }
            Ok(_) => {
                return Err("Error while parsing unimportant things: unexpected stuff".to_string())
            }
            Err(err) => return Err(format!("Error parsing: {}", err)),
        }
    }
    Ok(())
}

/// Parses data that should not be kept. Returns nothing
fn parse_unimportant<R: BufRead>(parser: &mut Reader<R>, buf: &mut Vec<u8>) -> Result<(), String> {
    use quick_xml::events::Event::*;
    let mut depth = 1;
    while depth > 0 {
        let parse = parser.read_event(buf);
        match parse {
            Ok(Text(_)) => {}
            Ok(Start(_)) => depth += 1,
            Ok(End(_)) => depth -= 1,
            Ok(Eof) => {
                return Err("Unexpected end of file in parsing unimportant things".to_string())
            }
            Ok(_) => {
                return Err("Error while parsing unimportant things: unexpected stuff".to_string())
            }
            Err(err) => return Err(format!("Error parsing: {}", err)),
        }
    }
    Ok(())
}
