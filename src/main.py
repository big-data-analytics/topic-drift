#!/bin/python3

from sklearn.feature_extraction.text import TfidfVectorizer
from csv import reader as CsvReader
from sklearn.cluster import KMeans
import sklearn
from sys import argv
from sklearn.decomposition import PCA
from matplotlib import pyplot
from itertools import chain
from collections import Counter
import pandas


NORMALISE_FEATURES = True
N_GRAM_SIZE = (2, 4)
TOP_N_CLUSTERS = 5
TOP_N_KEYWORDS = 5
N_SAMPLE_FEATURES = 15
N_SAMPLE_TITLES = 5
AMT_CLUSTERS = 15
AMT_CLUSTERING_REPETITIONS = 24
MAX_CLUSTERING_ITERATIONS = 300
AMT_THREADS = -1
OUTPUT_CSV = "output.csv"


class TitlesMetadata:
    def __init__(self, dataset_name):
        self.csv_file_name = "./" + dataset_name + ".csv"

    def __enter__(self):
        self.__csv_file = open(self.csv_file_name, "r")
        return self

    def __exit__(self, type, val, tb):
        self.__csv_file.close()

    def iter(self):
        self.__csv_file.seek(0)
        return CsvReader(self.__csv_file, delimiter=";")


def features_per_sample(samples):
    """
    The feature vector for a title is a sparse vector. This counts for each known word, i.e. words occurring in all titles, the occurrence in the current title. To compensate for common words, e.g. "the", "it", "and", etc., the counts are normalised using the number of titles they appear in as weight.
    This is called TF-IDF.
    """
    # use ngrams of n=3
    featurizer = TfidfVectorizer(
        use_idf=NORMALISE_FEATURES, max_df=0.80, stop_words=sklearn.feature_extraction.text.ENGLISH_STOP_WORDS.union(["data", "mining", "algorithm", "conference", "database", "databases"]), ngram_range=N_GRAM_SIZE)
    features = featurizer.fit_transform(samples)
    feature_names = featurizer.get_feature_names()

    return (features, feature_names)


def cluster_and_label(samples):
    # algorithm "full" is the only algorithm (of the 2) that works for sparse feature vectors
    clusterator = KMeans(n_clusters=AMT_CLUSTERS,
                         n_init=AMT_CLUSTERING_REPETITIONS, precompute_distances="auto", max_iter=MAX_CLUSTERING_ITERATIONS, random_state=42, n_jobs=AMT_THREADS, algorithm="full")
    predictions = clusterator.fit_predict(samples)
    centroids = clusterator.cluster_centers_

    return (centroids, predictions)


def args_from_cli() -> (str, int):
    try:
        arg = argv[1]
        if arg != "data_mining" and arg != "database_systems":
            print("Please provide either 'data_mining' or 'database_systems' as option.")
            exit(1)

        return (arg, int(argv[2]))
    except:
        print("Usage:", argv[0], "SUBSET", "OVERLAP_ONESIDE")
        exit(1)


def structure_data(samples, labels):
    return pandas.DataFrame(
        {"title": samples, "label": labels},
        index=[labels],
        columns=["title", "label"]
    )


def top_n_keywords(keywords):
    counts = Counter(keywords)
    return sorted(counts, key=lambda keyword: (-counts[keyword], keyword))[:TOP_N_KEYWORDS]


def analyse_top_n(centroids, feature_names, structured_result, interval, output_file):
    # Intrinsic sort of centroids such that features decrease in importance by increasing feature index.
    ordered_centroids = centroids.argsort()[:, ::-1]

    top_n_clusters = structured_result["label"].value_counts()[
        :TOP_N_CLUSTERS]

    print("Top", TOP_N_CLUSTERS, "topics info:")
    for cluster_idx in top_n_clusters.index:
        print("\nTopic " + str(cluster_idx) + ":")
        print("  Amount titles in cluster:", top_n_clusters[cluster_idx])
        print("  Prevalent keywords:", end="")  # GF
        keywords = top_n_keywords(chain.from_iterable(
            map(lambda feature_idx: feature_names[feature_idx].split(),
                ordered_centroids[cluster_idx][:N_SAMPLE_FEATURES])
        ))

        # Write begin year of interval
        output_file.write(str(interval[0]))
        output_file.write(";")
        # Write write cluster id
        output_file.write(str(cluster_idx))
        output_file.write(";")

        # Write count of the cluster
        output_file.write(str(top_n_clusters[cluster_idx]))

        # Write the individual words
        for keyword in keywords:
            output_file.write(";")
            output_file.write(keyword)

        output_file.write("\n")

        print(keywords)

        print("\n  Samples:")
        for title in structured_result.loc[cluster_idx]["title"][:N_SAMPLE_TITLES]:
            print("  -", title)


def main():
    with open(OUTPUT_CSV, "w") as output_file:
        args = args_from_cli()
        dataset_name = args[0]
        interval_overlap = args[1]

        interval_starts = range(1970 - interval_overlap, 2020, 10)
        interval_ends = range(1980 + interval_overlap, 2025, 10)

        intervals = zip(interval_starts, interval_ends)

        with TitlesMetadata(dataset_name) as titles_metadata:
            year_title_pairs = list(map(lambda csv_row: (
                int(csv_row[0]), csv_row[1]), titles_metadata.iter()))

            for interval in intervals:
                print("Interval " +
                      str(interval[0]) + " → " + str(interval[1]))
                relevant_titles = list(
                    map(
                        lambda pair: pair[1],
                        filter(
                            lambda pair: pair[0] >= interval[0] and pair[0] <= interval[1],
                            year_title_pairs[:]))
                )
                if len(relevant_titles) > 0:
                    (sample_features, feature_names) = features_per_sample(
                        relevant_titles)
                    (centroids, sample_labels) = cluster_and_label(sample_features)
                    structured_result = structure_data(
                        relevant_titles, sample_labels)

                    analyse_top_n(centroids, feature_names,
                                  structured_result, interval, output_file)
                else:
                    print("No data found.")
                print()


main()
